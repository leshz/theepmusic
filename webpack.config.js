const path = require('path');
var webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: path.join(__dirname, '/src/js/main.js'),
    output: {
        path: path.join(__dirname, '/public/'),
        filename: '[name].bundle.js'
    },
    devServer: {
        port: 8080,
        open: true,
        watchContentBase: true,
        contentBase: path.join(__dirname, "/public/"),
        proxy: {
            '/info': {
                target: 'http://localhost:8080/gallery.html',
                pathRewrite: { '^/info': '' },
            },
        }
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin()
        ]
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: {
                        minimize: true,
                        sourceMap: true
                    }
                },
                {
                    loader: 'postcss-loader'
                },
                {
                    loader: 'sass-loader',
                    options: {
                        minimize: true
                    }
                }
                ]
            }),
        }, {
            test: /\.pug$/,
            include: path.join(__dirname, 'src/html'),
            loaders: ['pug-loader']
        },
        {
            test: /\.(png|jpg|gif)$/,
            use: [{
                loader: 'file-loader',
                options: {}
            }]
        }
        ]
    },
    plugins: [
        new ExtractTextPlugin("css/styles.css"),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new HtmlWebpackPlugin({
            title: 'The Emerald Planet - Website',
            template: path.join(__dirname, '/src/html/index.pug'),
            inject: true,
            hash: true
        }),
        new HtmlWebpackPlugin({
            filename: 'gallery.html',
            title: 'The Emerald Planet - Website',
            template: path.join(__dirname, '/src/html/gallery.pug'),
            inject: true,
            hash: true
        })
    ]
}