/* Class Modal */
/**
 * @author Jeffer Barragan  <leshz@me.com>
 * @version 1.3
 */

export default class Modal {
  constructor(configurationObject) {
    if (typeof configurationObject === "undefined") configurationObject = {};
    this.baseClass = configurationObject.baseClass || "";
    this.selector = configurationObject.selector || "body";
    this.animationTime = configurationObject.animationTime || 1; // Seconds
    this.animationMiliseconds = this.animationTime * 1000;
  }

  open(template, callBackAfterShowModal) {
    const that = this;
    let $ModalContainer = document.getElementById("modal");

    function opening() {
      $ModalContainer.classList.add("active");
      const $Template = that.render(template);
      const animationString = `modalIn ${that.animationTime}s forwards`;
      $Template.style.animation = animationString;
      $ModalContainer.append($Template);
      that.closeEvent();
      if (callBackAfterShowModal) callBackAfterShowModal();
    }
    if (!$ModalContainer) {
      const $body = document.querySelector(this.selector);
      $body.classList.add("blurred");
      $body.append(
        Modal.createDomElement("<div id='modal' class='modal-overlay'></div>")
      );
    }
    $ModalContainer = document.getElementById("modal");
    if ($ModalContainer.classList.contains("active")) {
      this.close(true);
      setTimeout(() => {
        opening();
      }, that.animationMiliseconds);
    } else {
      opening();
    }
  }

  closeEvent() {
    const that = this;
    try {
      const element = document.querySelector(".close-modal");
      element.addEventListener("click", () => {
        that.close();
      });
    } catch (err) {
      console.info("🔥No Close Modal🔥");
    }
  }

  close(ContinueLoadModal) {
    const $modal = document.getElementById("modal");
    const animationString = `modalOut ${this.animationTime}s forwards`;
    $modal.querySelector(".modal-content").style.animation = animationString;
    document.querySelector(this.selector).classList.remove("blurred");
    setTimeout(() => {
      $modal.querySelector(".modal-content").remove();
      if (!ContinueLoadModal) $modal.classList.remove("active");
    }, this.animationMiliseconds);
  }

  render(template) {
    const templateToRender =
      template ||
      "<p>Contenido Por Default :D </p><button class='close-modal'>Cerrar</button>";
    const base = `<div class="modal-content ${this.baseClass}">{base}</div>`;
    const render = base.replace("{base}", templateToRender);
    return Modal.createDomElement(render);
  }

  static createDomElement(HTMLString) {
    const html = document.implementation.createHTMLDocument();
    html.body.innerHTML = HTMLString;
    return html.body.children[0];
  }
}
