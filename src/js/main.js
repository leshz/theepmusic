import "../sass/main.scss";
import "./lib/bxslider.js";
import "./lib/jquery.fullpage.min.js";
import Modal from "./modal";

var $triangles = document.querySelectorAll(".triangle");
var template =
  '<svg class="triangle-svg" viewBox="0 0 140 141">\n    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n      <polygon class="triangle-polygon"  points="70 6 136 138 4 138"></polygon>\n    </g>\n  </svg>';
Array.prototype.forEach.call($triangles, function ($triangle, index) {
  $triangle.innerHTML = template;
});


const modalOpen = window.location.pathname === "/";

const promo = new Modal({ animationTime: 1 });

const modalTemplate = `
  <img  src='https://s3-us-west-2.amazonaws.com/www.theepmusic.com/Cover+Casa+5.jpg' alt='Casa 5 Capricornio Cover Art'/>
  <a class="button-link spotify" target="_blank" href="https://open.spotify.com/track/7dk872HKMBTgLJw9Um62jB?si=dF8s72ZgQLuGm9fUD-Nbpw">Spotify</a>
  <a class="button-link apple" target="_blank" href="https://music.apple.com/co/album/casa-5-capricornio/1549965529?i=1549965532">Apple music</a>
  <button type="button" class="close-modal"> Cerrar </button>  
`;

function init() {
  setTimeout(function () {
    document.querySelector(".loader").classList.add("loaded");
    // if (modalOpen) promo.open(modalTemplate);
  }, 2000);
}

$("#fullpage").fullpage({
  menu: "#menu",
  fitToSection: true,
  scrollBar: true,
  navigation: false,
  navigationPosition: "left",
  slideSelector: ".song",
});
$(".slide").bxSlider({
  controls: false,
  pager: false,
  tickerHover: true,
  preloadImages: "all",
  mode: "fade",
  speed: 700,
  adaptiveHeight: true,
  infiniteLoop: true,
  randomStart: false,
  auto: true,
  autoStart: true,
});

function responsive() {
  if (window.innerWidth <= 769) {
    $(".btn-warp").addClass("hide");
  } else {
    $(".btn-warp").removeClass("hide");
  }
}

responsive();

$(window).resize(function () {
  responsive();
});

$(".resp-menu").click(function () {
  $(".btn-warp").toggleClass("hide");
});
window.onload = init;
